#include "stdafx.h"
#include <math.h>

BOOL init();
void close();

SDL_Window* gWindow = NULL;
SDL_Renderer* gRenderer = NULL;

SDL_Surface* screen = NULL;

BOOL init() {
	BOOL success = TRUE;
	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		printf("SDL could not initialize! SDL Error: %s\n", SDL_GetError());
		success = FALSE;
	}
	else {
		gWindow = SDL_CreateWindow("PL Graphics Lab", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
		if (gWindow == NULL) {
			printf("Window could not be created! SDL Error: %s\n", SDL_GetError());
			success = FALSE;
		}
		else {
			gRenderer = SDL_CreateRenderer(gWindow, -1, SDL_RENDERER_SOFTWARE);
			if (gRenderer == NULL) {
				printf("Renderer could not be created! SDL Error: %s\n", SDL_GetError());
				success = FALSE;
			}
			else {
				SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
			}
		}
	}
	return success;
}

void circle(int xc, int yc, int r, SDL_Renderer* renderer) {
	int circleX, circleY;
	int X, Y, delta, ep;
	Y = r;
	delta = Y - 1;
	X = 1;

	while (Y >= 0) {
		circleX = xc + X;
		circleY = yc + Y;

		SDL_RenderDrawPoint(renderer, circleX, circleY);

		circleX = xc - X;
		SDL_RenderDrawPoint(renderer, circleX, circleY);

		circleY = yc - Y;
		SDL_RenderDrawPoint(renderer, circleX, circleY);

		circleX = xc + X;
		SDL_RenderDrawPoint(renderer, circleX, circleY);

		int dh = abs((X + 1) * (X + 1) + Y * Y - r * r);
		int dv = abs(X * X + (Y - 1) * (Y - 1) - r * r);
		int dd = abs((X + 1) * (X + 1) + (Y - 1) * (Y - 1) - r * r);

		ep = dh - dv;
		
		if (ep < 0) {
			ep = dd - dh;
			
			if (ep < 0) {
				X++;
				Y--;
			}
			else {
				X++;
			}
		}
		else {
			ep = dd - dv;

			if (ep < 0) {
				X++;
				Y--;
			}
			else {
				Y--;
			}
		}
	}
}


void close() {
	SDL_DestroyRenderer(gRenderer);
	SDL_DestroyWindow(gWindow);
	gWindow = NULL;
	gRenderer = NULL;
	SDL_Quit();
}

int _tmain(int argc, _TCHAR* argv[])
{
	if (!init()) {
		printf("Failed to initialize!\n");
	}
	else {
		BOOL quit = FALSE;
		SDL_Event e;

		BOOL moveLeftLeg = TRUE;

		const int groundX = 0, groundY = 200, headRadius = 10, bodyHeight = 50, stepLength = 20, betweenHandsMax = 20;
		int x = 0, y = 100, neckX = 0, neckY = y + headRadius, feetDistance = 0, betweenHandsDistance = 0;
		int leftFoot = 0, rightFoot = 0, elbowX = 0, feetLowest = groundY;
		int leftHand = 0, rightHand = 0, handsLowest = neckY + 25, k = 1;


		while (!quit && x <= SCREEN_WIDTH) {
			while (SDL_PollEvent(&e) != 0) {
				if (SDL_QUIT == e.type) {
					quit = TRUE;
				}
			}

			SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
			SDL_RenderClear(gRenderer);
			SDL_SetRenderDrawColor(gRenderer, 0x00, 0x00, 0x00, 0xFF);

			// draw upper body
			circle(x, y, headRadius, gRenderer);
			SDL_RenderDrawLine(gRenderer, neckX, neckY, neckX, neckY + bodyHeight);
			SDL_RenderDrawLine(gRenderer, groundX, groundY, SCREEN_WIDTH, groundY);

			// draw legs
			
			// left leg
			SDL_RenderDrawLine(gRenderer, neckX, neckY + bodyHeight, leftFoot, feetLowest);
			// right leg
			SDL_RenderDrawLine(gRenderer, neckX, neckY + bodyHeight, rightFoot, feetLowest);

			// draw hands ----------------------------------------------------------------

			// left
			SDL_RenderDrawLine(gRenderer, neckX, neckY + 10, neckX - elbowX, neckY + 20);
			SDL_RenderDrawLine(gRenderer, neckX - elbowX, neckY + 20, neckX - elbowX, neckY + 30);

			// right 
			SDL_RenderDrawLine(gRenderer, neckX, neckY + 10, neckX + elbowX, neckY + 30);
			


			SDL_RenderPresent(gRenderer);
			SDL_Delay(20);

			x += 1;
			neckX += 1;
			elbowX += k;

			feetDistance += 1;

			if (moveLeftLeg) {
				leftFoot += 2;
			}
			else {
				rightFoot += 2;
			}


			if (elbowX == 20 || elbowX == 0) {
				k = -k;
			}

			if (feetDistance == stepLength) {
				feetDistance = 0;
				moveLeftLeg = !moveLeftLeg;
			}
			
		}
	}
	close();
	return 0;
}